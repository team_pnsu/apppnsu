/**
 * Listar el personal
 * @main RRHH
 * @author mfyance
 */

head.loadProxy(
    { common    : "Scripts/vendors/devextreme/css/dx.common.css"},
    { light     : "Scripts/vendors/devextreme/css/dx.light.css"},
    { csspicker : "Scripts/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"},    
    { jszip     : "Scripts/vendors/jszip/dist/jszip.min.js" },    
    { select2   : "Scripts/vendors/select2/dist/js/select2.full.min.js" },
    { jspicker  : "Scripts/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" },
    { moment  : "Scripts/vendors/moment/min/moment.min.js" },
    { momentz  : "Scripts/vendors/moment-timezone/builds/moment-timezone-with-data.min.js" },
    function(){
        Globalize.culture("es-PE");
        modulo.init();
    }
);

var modulo = (function($, undefined) {
    "use strict";
    var events, suscribeEvents, beforeCathDom, catchDom, afterCathDom,dom,dx, fn, initialize, st, method , complete, error, params, success, url;

    st = {
        gridContainer       : '.grid__contenedor',
        btnIniciar          : '<button type="button" class="btn btn-info">Iniciar</button>',
        url:{
            customers       : PNSU.servicesPath + "customers",
            employees       : PNSU.servicesPath + "employees",
            asignaciones    : PNSU.servicesPath + "asignaciones",
            formats         : PNSU.servicesPath + "formats"
        }
    };
    dom = {};
    dx = {};
    beforeCathDom = function(){
        dx.columnsFilters = {
            allowFiltering: true, 
            filterOperations: 'contains',
            selectedFilterOperation: 'contains'
        }

        dx.columnsTmpEmployees = [
            $.extend(
                { 
                    dataField: "FirstName", 
                    caption: "Nombre", 
                    width: "15%", 
                    dataType:'int'                   
                },
                dx.columnsFilters
            ),
            $.extend(
                { 
                    dataField: "Position", 
                    caption: "Rol", 
                    width: "15%", 
                    dataType:'int'
                },
                dx.columnsFilters
            ),          
            $.extend(
                { 
                    dataField: "DocType", 
                    caption: "Tipo Doc", 
                    width: "15%", 
                    dataType:'int'
                },
                dx.columnsFilters
            ),
            $.extend(
                { 
                    dataField: "idMarital", 
                    caption: "E. Civil", 
                    width: "15%", 
                    dataType:'int',
                    lookup: { 
                        dataSource: new DevExpress.data.CustomStore({
                            load: function (loadOptions) {
                                var dFormats = new $.Deferred()
                                url = st.url.formats;
                                params = {};
                                success = function(oResult){
                                    dFormats.resolve(oResult.data)
                                };
                                error = function(e) {
                                    console.error('dFormats', e);
                                };
                                APP.sb.createServices(url, params, method , success, complete, error)
                                return dFormats.promise()
                            },
                            byKey: function(key){
                                return { id: key.id }
                            }
                        }), 
                        valueExpr: 'id',
                        displayExpr: 'format'
                    }
                },
                dx.columnsFilters
            ),
            $.extend(
                { 
                    dataField: "Phone", 
                    caption: "Teléfono", 
                    width: "15%", 
                    dataType:'number'
                },
                dx.columnsFilters
            ),
            $.extend(
                { 
                    dataField: "BirthDate", 
                    caption: "Nac.", 
                    width: "15%", 
                    dataType:'date',
                    // format: 'dd/MM/yyyy',
                    customizeText: function (args) {
                        if(moment(args.value).isValid()){
                            return moment(args.value).format("DD/MM/YYYY")
                        }else{
                            return moment(args.value, "DD/MM/YYYY").format("DD/MM/YYYY")
                        }
                    }
                },
                dx.columnsFilters
            ),
            {
                caption: 'Acciones',
                width: "10%",
                allowFiltering: false,
                allowEditing: false,
                cellTemplate: function (container, options) {
                    $(st.btnIniciar).appendTo(container);
                }
            }
        ]

        dx.myDataSource = new DevExpress.data.DataSource({
            store: new DevExpress.data.CustomStore({
                load: function(loadOptions) {
                    
                    /**
                     * @var {ajaxParams} Obtener parámetros para filtrar la consulta
                     * @var skip : filas omitidas
                     * @var take : filas vistas
                     * @var x    : página vista
                     * @var y    : # grupos
                     * @returns {object}               
                     */
                    var ajaxParams = new fn.dxRefreshParams()                    
                        , dEmployees = new $.Deferred()
                        , skip = loadOptions.skip?loadOptions.skip:0
                        , take = loadOptions.take?loadOptions.take:10
                        , x = skip/take +1
                        , y = take
                        , oInicioFin = { pInicio: (x - 1) * y + 1, pFin: x * y }
                        , newAjaxParams = $.extend(dx.ajaxParams, oInicioFin)
                        , filterOptions = loadOptions.filter ? loadOptions.filter : []
                        , sortOptions   = loadOptions.sort ? loadOptions.sort : []
                    ;

                    /* @type {function} tempArr: Envia filtros y ordenamiento
                     * @prop {vJson} Agrega una nueva propiedad
                     */
                    var tempArr = APP.sb.dxAsyncParameters(dom.gridContainer,filterOptions,sortOptions)
                        newAjaxParams.vJson = JSON.stringify(tempArr)
                        console.log('Data Parameters:' , newAjaxParams)

                    /* @type {function} Crea el servicio bajo Ajax Promise
                     * @var {url} Dirección del servicio
                     * @var {success} Promesa para el done del ajax
                     * @var {error} Promesa para el error del ajax
                     * @returns {void}
                     */
                    url = st.url.employees;                    
                    params = JSON.stringify(newAjaxParams);
                    success = function(oResult) {
                        dEmployees.resolve(oResult.data, { totalCount: oResult.cantItems });
                    };
                    error = function(e) {
                        console.error('dEmployees', e);
                    };
                    APP.sb.createServices(url, params, method , success, complete, error)
                    return dEmployees.promise()
                },
                byKey: function(key) {
                    // return $.getJSON(SERVICE_URL + "/" + encodeURIComponent(key));
                },       
                insert: function(values) {
                    // return $.post(SERVICE_URL, values);
                },
                update: function(key, values) {
                    // return $.ajax({
                    //     url: SERVICE_URL + "/" + encodeURIComponent(key),
                    //     method: "PUT",
                    //     data: values
                    // });
                },
                remove: function(key) {
                    // return $.ajax({
                    //     url: SERVICE_URL + "/" + encodeURIComponent(key),
                    //     method: "DELETE",
                    // });
                }
            }),
            onChanged: function() {
            }
        });
        
        dx.gridOptions  = {
            dataSource: dx.myDataSource,
            columns:    dx.columnsTmpEmployees,
            masterDetail: {
                autoExpandAll:false,
                enabled: true,
                template: function ($container, options) {
                    var row = options.data ? options.data : {}
                    $container.append($(
                        "<div class='table-responsive'>\
                            <table class='table button-clean table__master'>\
                                <tbody>\
                                    <tr class='text-center'>\
                                        <td class='table--no-border'>\
                                            <table class='table text-left table--is-notstyle'>\
                                                <tr><td class='table--no-border'>\
                                                    <h4><i class='fa fa-comment pr--10'></i>Información Adicional:</h4>\
                                                    </td></tr>\
                                                <tr><td><textarea class='textarea' disabled>" + row.Address + "</textarea></td></tr>\
                                            </table>\
                                        </td>\
                                        <td class='table--no-border'>\
                                            <table class='table text-left table--is-notstyle'>\
                                                <tr><td class='table--no-border'>\
                                                    <h4><i class='fa fa-comment pr--10'></i>Información Adicional:</h4>\
                                                    </td></tr>\
                                                <tr><td><textarea class='textarea' disabled>" + row.Notes + "</textarea></td></tr>\
                                            </table>\
                                        </td>\
                                    </tr>\
                                    <tr>\
                                        <td colspan='2' class='table--no-border'>\
                                            <table class='table button-clean'>\
                                                <thead><tr class='text-center'><td>Fecha de Inicio</td><td>Hora de Inicio</td><td>Fecha de fin</td><td>Hora de fin</td><td>Total minutos</td><td>Total dias</td></tr></thead>\
                                                <tbody>\
                                                    <tr class='color-red text-center'>\
                                                        <td class='color--blue'>" + row.HireDate + "</td>\
                                                        <td class='color--blue'>" + row.HoraSalida + "</td>\
                                                        <td class='color--blue'>" + row.BirthDate + "</td>\
                                                        <td class='color--blue'>" + row.HoraRetorno + "</td>\
                                                        <td class='color--blue'>" + row.TotalMinutos + "</td>\
                                                        <td class='color--blue'>" + row.TotalDias + "</td>\
                                                    </tr>\
                                                </tbody>\
                                            </table>\
                                        </td>\
                                    </tr>\
                                    <tr>\
                                        <td colspan='2' class='table--no-border'>\
                                            <textarea id='" + row.ID + "' maxlength='400' class='form-control text-counter' placeholder='Ingresa la razón de la Aprobación o Rechazo'>"
                                                + row.City +
                                            "</textarea>\
                                        </td>\
                                    </tr>\
                                </tbody>\
                            </table>\
                        </div>"
                    ));
                    
                    /**
                     * Solución bug en el textarea
                     */
                    $('#' + row.ID).off('keypress').on('keypress', function () {
                        dom.gridContainer.data( row.ID, $.trim($(this).val()));
                    }).trigger('keydown');
                }
            },
            onContentReady: function(info){
               
                APP.sb.dxBtnClearFilters(dom.gridContainer)
                APP.sb.dxBtnRefreshData(dom.gridContainer)
                APP.sb.dxCheckBox(dom.gridContainer)
            }
        }
    }
    catchDom = function(){       
        dom.gridContainer   = $(st.gridContainer);
    };
    afterCathDom = function(){
        dom.gridContainer.dxDataGrid(
            $.extend( new APP.sb.dxConfigGridOptions() , dx.gridOptions )
        )
    };
    suscribeEvents = function () {
    };
    events = {       
    };
    fn = {
        dxRefreshParams : function(){
            var ajaxParams;
            ajaxParams = {
                xidUsuario: 1,
                xidPersona: 1,
                xidArea: 0,
                pTipoBandeja: 0,
                pCondicion: 'AND',
                pInicio: "1",
                pFin: "100",
                filaInicial: "1",
                filaFinal: "31"
            }
        }
    };
    initialize = function() {
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
})(jQuery);