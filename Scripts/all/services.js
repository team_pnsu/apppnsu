/**
 * Load all functions to this proyect
 * @main All js
 * @author mfyance
 */
window.WS = (function($, undefined) {
    "use strict";
    var events, suscribeEvents, beforeCathDom, catchDom, afterCathDom,dom, fn, initialize, st, sandbox, ajax, service;
    var oCol,oFilter,oFilterDate, dataType,i,j,s,t,colName,colType,colValue,colDataType,gridColumns,operators,tempArr,tempJson,defColData,filters,sorts,oSort;

    st = {
       
    };
    dom = {};
    beforeCathDom = function(){
    },
    catchDom = function(){   
    };
    afterCathDom = function(){
    };
    suscribeEvents = function () {
    };
    events = {
       
    };
    fn = {
    };
    sandbox = {
    	
    };

    initialize = function() {
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };    
    return {
        init: initialize,
        sb: sandbox
    };
})(jQuery);
WS.init();