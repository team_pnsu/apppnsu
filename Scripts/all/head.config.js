head.loadProxy = function (/*aDeps, fFnc*/) {
    var args = arguments, len = args.length, i;
    for (i = 0; i < len; i++) {
        var arg = args[i];
        var version, path;
        if (typeof (arg) !== 'function') {
            for (var lbl in arg) {                      
                arg[lbl] = PNSU.basePath + arg[lbl] + '?v=' ;                      
                continue; 
            }
        }
    }
    head.load.apply(head, args);
};