/**
 * Load all functions to this proyect
 * @main Scripts/all
 * @author mfyance
 */
window.APP = (function($, undefined) {
    "use strict";
    var events, suscribeEvents, beforeCathDom, catchDom, afterCathDom,dom, fn, initialize, st, sandbox, ajax, service, type;
    var oCol,oFilter,oFilterDate, dataType,i,j,s,t,colName,colType,colValue,colDataType,gridColumns,operators,tempArr,tempJson,defColData,filters,sorts,oSort;

    st = {
        html        : 'html',
        dxHeader    : '.dx-datagrid-header-panel',
        chkExportar : '.dx-menu-item-content>.dx-datagrid-checkbox-size',
        gridType    : 'dx-grid',
        btnClear    : '.grid__btn-clear',
        btnRefresh  : '.grid__btn-refresh',
        btnSelector : '.grid__selector',
        loader      : '.loader'
    };
    dom = {};
    beforeCathDom = function(){
    },
    catchDom = function(){   
        dom.html        = $(st.html);
        dom.btnClear    = $(st.btnClear);
        dom.btnRefresh  = $(st.btnRefresh);
        dom.btnSelector = $(st.btnSelector);
        dom.loader      = $(st.loader);
    };
    afterCathDom = function(){
    };
    suscribeEvents = function () {
        dom.html.on('click', st.chkExportar, events.controlSelectExport)
    };
    events = {
         /**
         * Functionally to checkbox. True = Show the check to export them
         * @param {_this} Get the context
         * @returns {void}
         */
        controlSelectExport: function(_this){
            var gridContainer = $(_this.currentTarget).closest(dom.html).find("[data-type='"+st.gridType+"']");
            console.log(gridContainer)
            var dataGrid = gridContainer.dxDataGrid('instance');
            setTimeout(function(){
                var status = $(_this.currentTarget).attr('aria-checked');
                dataGrid.option({ selection: { mode: status === "true" ? 'multiple' : 'none' } });
            },400)           
        }
    };
    fn = {
    };
    sandbox = {
    	/**
         * Web Service in Ajax Call
         * @param {url}
         * @param {params}
         * @param {method}
         * @returns {ajax}
         */
    	configAjax : function( url, params, method ){
            type = (method != undefined) ? method : "POST"
		    var ajax = $.ajax({
		        type: type,
		        contentType: 'application/json; charset=utf-8',
		        dataType: 'json',
		        url: url,
		        data: params,
		        timeout: 30000,
		        cache: false,
                beforeSend: function(){
                    dom.loader.hide()
                }
		    });
		    return ajax;
		},

		/**
		 * Create service with Promise Methods
		 * @param {url} Service's Url 
		 * @param {params} Params to filter consult into service
         * @param {method} Return method ajax
		 * @param {success} Return data after ajax call success
		 * @param {complete} Return data after ajax call complete
		 * @param {error} Return data after ajax call error
		 * @returns {void}
		         */
		createServices : function(url, params, method, success, complete, error){ 
		    service = sandbox.configAjax(url, params, method);
		    if ($.isFunction(success) === true) {
		      service.done(success);
		    }
		    else if ($.isFunction(complete) === true) {
		      service.always(complete);
		    }
		    else if($.isFunction(error) === true) {
		      service.fail(error);
		    }
		},

        /**
         * Create refresh button into Grid
         * @param {gridContainer} Grid container
         * @returns {void}
         */
        dxBtnRefreshData: function(gridContainer){            
            dom.btnRefresh.dxButton({
                icon: 'fa fa-refresh',
                onClick: function(info) {                    
                    var dataGrid = gridContainer.dxDataGrid('instance');
                    dataGrid.refresh()
                }
            }).prependTo(st.dxHeader);
            setTimeout(function () {
                dom.btnRefresh.attr('title', dom.btnRefresh.data('title'))
            },10)
        },

        /**
         * Create clear button into Grid
         * @param {gridContainer} Grid container
         * @returns {void}
         */
        dxBtnClearFilters: function(gridContainer){            
            dom.btnClear.dxButton({
                icon: 'fa fa-eraser',
                onClick: function(info) {
                    console.log(info)
                    var dataGrid = gridContainer.dxDataGrid('instance');
                    dataGrid.clearFilter()
                }
            }).prependTo(st.dxHeader);
            setTimeout(function () {
                dom.btnClear.attr('title', dom.btnClear.data('title'))
            },10)
        },

        /**
         * Create checkbox about row sized
         * @param {gridContainer} Grid container
         * @returns {void}
         */
        dxCheckBox: function (gridContainer) {
            dom.btnSelector.dxCheckBox({
                value: false,
                text: 'Mostrar tamaños de filas',
                onValueChanged: function (info) {
                    var dataGrid = gridContainer.dxDataGrid('instance');
                    dataGrid.option({
                        pager: { showPageSizeSelector: info.value }
                    });
                    // dataGrid.repaint();
                }
            }).appendTo(gridContainer);
        },
        
        /**
         * Create PNSU's Async Parameters
         * @param {gridContainer} Grid container
         * @param {filterOptions} Get filterOptions into grid container
         * @param {sortOptions} Get sortOptions into grid container
         * @returns {void}
         */
        dxAsyncParameters: function(gridContainer, filterOptions, sortOptions){
            gridColumns = gridContainer.dxDataGrid('instance').option('columns');
            operators   = { contains: 'like' };
            tempArr     = [];
            tempJson    = {};
            defColData  = { col:"", ord:"", value:"", type:"like", datatype:"" };
            filters     = ($.isArray(filterOptions[0])) ? filterOptions : [filterOptions];
            sorts       = ($.isArray(filters[0])) ? sortOptions : [sortOptions];
          
            for (i in gridColumns) {
                oCol         = gridColumns[i];
                colDataType  = oCol.dataType ? oCol.dataType : 'string';
                
                /* 
                 * Recorriendo los Filtros
                 */
                for (var j in filters) {
                    oFilter  = filters[j];
                    oFilterDate = oFilter[0];
                    
                    if ($.isArray(oFilter)) {
                        colName  = oFilter[0];
                        colType  = operators[oFilter[1]];
                        colValue = oFilter[2];
                        /* 
                         * Si item filtro es array y es igual a la columna iterada
                         */
                        if (colValue != undefined && colName === oCol.dataField) {
                            if (colDataType === "date"){
                                var data =  moment(colValue).tz('America/Lima')
                                colValue = moment(data._d).format("DD-MM-YYYY")
                            } 
                            tempJson[colName] = $.extend({}, 
                                tempJson[colName]?tempJson[colName]:defColData, 
                                {col:colName, value:colValue, type:colType, datatype:colDataType} 
                            );
                        }
                    }

                    if ($.isArray(oFilter[0])){
                        colName    = oFilterDate[0];
                        colType    = "like";
                        colValue   = oFilterDate[2];
                        /* 
                         * Si item filtro es array y es igual a la columna iterada
                         */
                        if (colValue != undefined  && colName === oCol.dataField) {
                            if (colDataType === "date"){                                
                                colValue = moment(colValue).format("DD-MM-YYYY")
                            }
                            tempJson[colName] = $.extend( {}, 
                                tempJson[colName] ? tempJson[colName] : defColData,
                                {col:colName, value:colValue, type:colType, datatype:colDataType}
                            );
                        }
                    }
                }
                /* 
                 * Recorriendo los ordenamientos
                 */
                for (s in sorts) {
                    oSort       = sortOptions[s];
                    colName     = oSort.selector;
                    colValue    = oSort.desc ? 'DESC' : 'ASC';
                    
                    if ( colName == oCol.dataField) {
                        tempJson[colName] = $.extend({}, 
                            tempJson[colName]?tempJson[colName]:defColData, 
                            { col:colName , ord:colValue , datatype:colDataType }
                        );
                    }
                }
            }

            for (t in tempJson){ 
                tempArr.push(tempJson[t]);
            }
            return tempArr
        },

        /**
         * Create Config to Data grid
         * @returns {gridOptions} Config the options to  Data Grid
         */
        dxConfigGridOptions : function () {
            var gridOptions = {               
                selection: { 
                    mode: "none" ,
                    allowSelectAll: true 
                },
                allowColumnReordering: true,
                allowColumnResizing: true,
                hoverStateEnabled: true,
                export: { 
                    fileName: "DataGrid",
                    enabled: true, 
                    allowExportSelectedData: true,
                    excelFilterEnabled: true                
                },
                filterRow: { 
                    visible: true,
                    showOperationChooser: false,
                    applyFilter: "auto"
                }, 
                headerFilter: { 
                    visible: false 
                },
                sorting: { 
                    mode: "multiple" 
                },
                searchPanel: { 
                    visible: false,
                    width: 250 
                },
                paging: {
                    enabled: true,
                    pageSize: 5,
                    pageIndex: 0
                },
                pager: { 
                    visible: true, 
                    showPageSizeSelector: false, 
                    allowedPageSizes: [2, 5, 10],
                    showInfo: true,
                    showNavigationButtons: true
                },
                onRowCollapsed : function () { 
                    console.log("onRowCollapsed->args:", arguments)  
                },
                onRowCollapsing: function () { 
                    console.log("onRowCollapsing->args:", arguments) 
                },
                onRowExpanded  : function () { 
                    console.log("onRowExpanded->args:", arguments)   
                },
                onRowExpanding : function (e) { 
                    console.log("onRowExpanding->args:", arguments)  
                },
                onSelectionChanged: function (e) {
                    e.component.collapseAll(-1);
                    e.component.expandRow(e.currentSelectedRowKeys[0]);
                }
            }
            return gridOptions
        },

        /**
         * Create Config to Chart
         * @returns {gridOptions} Config the options to  Chart
         */
        dxConfigChartOptions : function () {
            var chartOptions = {
                palette: 'Soft',
                commonSeriesSettings: { 
                    argumentField: "day", 
                    type: "line" 
                },
                size: { 
                    height: 500 
                },
                legend: {
                    visible: true,
                    border:{
                        visible:true,
                        dashStyle: 'dash'
                    },
                    font: {
                        color: '#222d32',
                        size: 15,
                        family: 'Source Sans Pro'
                    },
                    verticalAlignment: "top",
                    horizontalAlignment: "right"
                },
                commonPaneSettings: { 
                    border: { 
                        visible: true, 
                        width: 1
                    }
                },
            }
            return chartOptions
        }
    };

    initialize = function() {
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };    
    return {
        init: initialize,
        sb: sandbox
    };
})(jQuery);
APP.init();