window.GLOBAL = {};
var enviroment = (function($, undefined) {
    "use strict";   
    var i,parser,xmlDoc,settings, events,fn, initialize,suscribeEvents;
    
    suscribeEvents = function () {
        fn.ajaxPNSU('Web.config', events.parserData)
    };
    events = {
        parserData: function(xhr){
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(xhr.responseText,"text/xml");
            settings = xmlDoc.getElementsByTagName("add")
            for(i = 0 ; i < settings.length; i++){
                if (settings[i].getAttribute("key") === "enviroment") { 
                    GLOBAL.enviroment = settings[i].getAttribute("value")
                }
                if (settings[i].getAttribute("key") === "staticVersion") { 
                    GLOBAL.version = settings[i].getAttribute("value")
                }
                if (settings[i].getAttribute("key") === "mainPath") { 
                    GLOBAL.mainpath = settings[i].getAttribute("value")
                }
                if (settings[i].getAttribute("key") === "localPort") { 
                    GLOBAL.localport = settings[i].getAttribute("value")
                }
                if (settings[i].getAttribute("key") === "localService") { 
                    GLOBAL.localservice = settings[i].getAttribute("value")
                }

            }
        }
    };
    fn = {
        createConfig: function(enviroment,version,mainpath, localport,localservice){
           
            window.Config = {}
            Config = {
                environment : enviroment,
                version : version ,
                minified : "",
                port : {
                    server :  localport,
                    services : localservice
                },
                paths:{
                    base    : mainpath + ":"+ localport + "/",
                    statics : mainpath + ":"+ localport + "/",
                    vendors : mainpath + ":"+ localport + "/",
                    services: mainpath + ":"+ localservice + "/"
                }
            }
            if(Config.environment != "LOCAL"){
                Config.minified = ".min"
            }
             console.log(Config)
        },
        ajaxPNSU : function(url, callback){
            var xhr;     
            if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
            else {
                var versions = ["MSXML2.XmlHttp.5.0", 
                                "MSXML2.XmlHttp.4.0",
                                "MSXML2.XmlHttp.3.0", 
                                "MSXML2.XmlHttp.2.0",
                                "Microsoft.XmlHttp"]

                 for(var i = 0, len = versions.length; i < len; i++) {
                    try {
                        xhr = new ActiveXObject(versions[i]);
                        break;
                    }
                    catch(e){}
                }
            }
             
            xhr.onreadystatechange = stateXhr;
             
            function stateXhr() {
                if(xhr.readyState < 4) {
                    return;
                }         
                if(xhr.status !== 200) {
                    return;
                }
                if(xhr.readyState === 4) {
                    callback(xhr);
                }           
            }     
            xhr.open('GET', url, true);
            xhr.send('');
        }
    };
    initialize = function() {
       suscribeEvents()
    };
    return {
        init: initialize
    };
})();
enviroment.init()