var gulp 		  = require('gulp'),
	minifyCss 	  = require('gulp-minify-css'),
	concatCss 	  = require('gulp-concat-css'),
	concatJs 	  = require('gulp-concat'),
	rename 		  = require('gulp-rename'),
	rimraf 		  = require('gulp-rimraf'),
	browserSync   = require('browser-sync'),
	nodemon 	  = require('gulp-nodemon'),
	runSequence   = require('run-sequence'),
	uglify 		  = require('gulp-uglify'),
	git 		  = require('git-rev-sync'),
	path    	  = require('./paths'),
	global 		  = require('./ports');

/**
 * Tarea para ejecutar el browserSync, una vez ejecutada la tarea nodemon
 * @task {gulp server} Inicia el servidor local
 */
gulp.task('server', ['nodemon'], function () {
	browserSync({
		proxy: "http://localhost:" + global.server,
		files: ["../**/*.*"],
		browser: "google chrome",
        port: global.server
	})
})

/**
 * Tareas para detectar el último commit realizado en GIT
 * @task {gulp git:version} Obtiene la versión abreviada del commit
 */
global.version = '.'+git.short()
gulp.task('git:version', function(){
	version = git.short()
	console.log("\x1b[33mStaticVersion: "+"\x1b[31m"+version+"\x1B[0m")
})

/**
 * Tarea para ejecutar el servidor express mediante el script server.js
 * @task {gulp nodemon}
 */
gulp.task('nodemon', function (cb) {
	var called = false;
	return nodemon({
		script: 'server.js'
	})
	.on('start', function () {
		if (!called) {
			called = true;
			cb();
		}
	})
	.on('restart', function () {
		setTimeout(function () {
			browserSync.reload({
				stream: false
			});
		}, 1000)
	})
	.on('crash', function(){
		console.log("Error en la tarea: gulp watch'")
	});
})

/**
 * Tareas para generar archivos minificados c/ versión
 * @task {gulp js} Limpia los archivos generados dinámicamente y los vuelve a generar
 * @task {gulp js:all} Concatena + Versiona + Minifica a los archivos dentro de ALL
 * @task {gulp js:module} Concatena + Versiona + Minifica a TODO (excepto ALL) los módulos
 * @task {gulp js:this} Concatena + Versiona + Minifica determinado módulo. 
 * Ejm: gulp js:this -thismodule/module.js 
 */
gulp.task('js', ['clean:js'] , function (done) {
	runSequence('js:all', 'js:module', function () {
		console.log("\x1b[35mArchivos JS generados"+"\x1b[33m All & Módulos" +"\x1B[0m");
	});
	done();
});
gulp.task('js:all', function(){
	return gulp.src(path.scripts.allJs)
		.pipe(concatJs('all.min' + global.version + '.js'))
		.pipe(uglify())
		.pipe(gulp.dest(path.scripts.all))
})
gulp.task('js:module', function(){
	return gulp.src(path.scripts.modulosJs)
		.pipe(rename({ suffix: '.min' + global.version }))
		.pipe(uglify())
		.pipe(gulp.dest(path.scripts.modulos))
})
gulp.task('js:this', function() {
    _path = process.argv[3].substr(1, process.argv[3].length);
    splitPath = _path.split('/');
    module = splitPath[0]+'/';
    file = splitPath[splitPath.length-1].split('.');
    js = file[0]+'.'+file[1];
    mypath = path.scripts.modulos + module + js
	
	console.log("\x1b[32mMódulo JS generado: \x1b[36m"+module+file[0]+".min"+global.version+".js"+"\x1b[0m");
    return gulp.src(path.scripts.modulos + module + js)
    	.pipe(concatJs(file[0] + '.min' + global.version + '.js'))
		.pipe(uglify())
		.pipe(gulp.dest(path.scripts.modulos + module))
})

/**
 * Tareas para generar archivos minificados c/ versión
 * @task {gulp css} Limpia los archivos generados dinámicamente y los vuelve a generar
 * @task {gulp css:all} Concatena + Versiona + Minifica a los archivos dentro de ALL
 * @task {gulp css:module} Concatena + Versiona + Minifica a TODO (excepto ALL) los módulos
 * @task {gulp css:this} Concatena + Versiona + Minifica determinado módulo. 
 * Ejm: gulp css:this -thismodule/module.css 
 */
gulp.task('css',  ['clean:css'] , function (done) {
	runSequence('css:all', 'css:module', function () {
		console.log("\x1b[35mArchivos CSS generados"+"\x1b[33m All & Módulos" +"\x1B[0m");
	});
	done();	
})
gulp.task('css:all', function() {
	return gulp.src(path.content.allCss)
		.pipe(concatCss('all.min' + global.version + '.css'))
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(gulp.dest(path.content.all))
})
gulp.task('css:module', function() {
	return gulp.src(path.content.modulosCss)		
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(rename({ suffix: '.min' + global.version }))
		.pipe(gulp.dest(path.content.modulos))
})
gulp.task('css:this', function() {
    _path = process.argv[3].substr(1, process.argv[3].length);
    splitPath = _path.split('/');
    module = splitPath[0]+'/';
    file = splitPath[splitPath.length-1].split('.');
    css = file[0]+'.'+file[1];
    mypath = path.content.modulos + module + css
	
	console.log("\x1b[32mMódulo CSS generado: \x1b[36m"+module+file[0]+".min"+global.version+".css"+"\x1b[0m");
    return gulp.src(path.content.modulos + module + css)
    	.pipe(concatCss(file[0] + '.min' + global.version + '.css'))
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(gulp.dest(path.content.modulos + module))
})

/**
 * Tareas para eliminar archivos generados dinámicamente
 * @task {gulp clean:css} Elimina todo los archivos generados
 * @task {gulp clean:css:all} Elimina all.min.[version].css
 * @task {gulp clean:css:module} Elimina [modulo].min.[version].css
 */
gulp.task('clean:css', function (cb) {
	runSequence('clean:css:all', 'clean:css:module' ,cb)
})
gulp.task('clean:css:all', function () {
	return gulp.src(path.content.allMinified, { read: false })
		.pipe(rimraf({ force: true }))
})
gulp.task('clean:css:module', function () {
	return gulp.src(path.content.modulosMinified, { read: false })
		.pipe(rimraf({ force: true }))
})

/**
 * Tareas para eliminar archivos generados dinámicamente
 * @task {gulp clean:js} Elimina todo los archivos generados
 * @task {gulp clean:js:all} Elimina all.min.[version].js
 * @task {gulp clean:js:module} Elimina [modulo].min.[version].js
 */
gulp.task('clean:js', function (cb) {
	runSequence('clean:js:all', 'clean:js:module' ,cb)
})
gulp.task('clean:js:all', function () {
	return gulp.src(path.scripts.allMinified, { read: false })
		.pipe(rimraf({ force: true }))
})
gulp.task('clean:js:module', function () {
	return gulp.src(path.scripts.modulosMinified, { read: false })
		.pipe(rimraf({ force: true }))
})

/**
 * Tarea para eliminar los css y js creados dinámicamente.
 * @task {gulp clean} 
 */
gulp.task('clean', function (cb) {
	runSequence('clean:js', 'clean:css' ,cb)
})

/**
 * Tarea para crear css y js [concat + minified + versioned]
 * @task {gulp esti}
 */
gulp.task('esti', function (cb) {
	runSequence('js', 'css' ,cb)
})