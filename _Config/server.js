/**
 * Initialize Local Services/Server Express
 * @main _Config
 * @author mfyance
 */

var express     = require('express'),
    fs          = require('fs'),
    serveStatic = require('serve-static'),
    ports       = require('./ports');

/**
 * @vars : Create express
 */
var appServer   = express(),
    appServices = express();

/**
 * @vars : Constant the app
 */
var constants = {
  "ERR": {
    "File_Not_Found": {
      "ERR_ID": "ERR_001",
      "description": "The request file was not found"
    },
    "ENOENT": {
      "ERR_ID": "ERR_002",
      "description": "Permission denined. Please create parent folder, if it doesn't exist"
    }
  },
  "SUCCESS": {
    "response": "success"
  }
}

/**
 * @vars : Directives
 */
var call_param    = '/',
    isStatic      = __dirname + '../..',
    portServices  = ports.services,
    portServer    = ports.server;

/**
 * Allows access to all origin
 */
appServices.all("*", function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept')
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
  next();
});

/**
 * Returns a JSON service response by fetching the files in ./static_files/json folder
 */
appServices.get('*', function (req, res) {
  try {
    call_param = req.params[0];
    res.setHeader('Content-Type', 'application/json');
    res.sendFile(__dirname + '/services' + call_param + '.json', {}, function (err) {
      res.status(404).end(JSON.stringify(constants.ERR.File_Not_Found));
    });
  } catch (ex) {
    res.status(404).end(ex.message);
  }
});

/**
 * POST creates a file at the location, with the request body and returns response from postresp folder
 */
appServices.post('*', function (req, res) {
  try {
    call_param = req.params[0];
    res.setHeader('Content-Type', 'application/json');
    res.sendFile(__dirname + '/services' + call_param + '.json', {}, function (err) {
      res.status(404).end(JSON.stringify(constants.ERR.File_Not_Found));
    });
  } catch (ex) {
    res.status(404).end(ex.message);
  }
});

/**
 * Sucess response for all non GET requests
 */
appServices.all('*', function (req, res) {
  try {
    res.status(200).end(JSON.stringify(constants.SUCCESS));
  } catch (ex) {
    res.status(404).end(ex.message);
  }
});

/**
 * Run the application on the port services
 */
if (appServices.get('env')) {
  portServices = process.env.PORT || portServices;
}

/**
 * Start Local Services
 */
appServices.listen(portServices, function () {
  console.log('Starting Local Services: ' + portServices);
});

/**
 * Start Local Server
 */
appServer.use(function (req, res,next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next()
})

appServer.use(serveStatic(isStatic))
appServer.listen(portServer, function () {
  console.log('Starting Local Server: : ' + portServer);
});