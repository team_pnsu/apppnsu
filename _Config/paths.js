/**
 * Rutas para la compilación Gulp
 * @main _Config
 * @author mfyance
 */

var Path = {base : {}, config : {}, content : {} , scripts:{}, dest : {}};

/**
 * Ruta base del proyecto
 * @property base
 * @type String
 */
Path.base                         = __dirname +'/../';
/**
 * Ruta base de la carpeta frontend
 * @property base
 * @type String
*/
Path.config.base            	= Path.base + '_Config/';
Path.config.styles              = Path.config.base + 'styles/';

Path.content.base               = Path.base + 'Content/';
Path.content.css                = Path.content.base + 'css/';
Path.content.font               = Path.content.base + 'fonts/';
Path.content.img                = Path.content.base + 'img/';

Path.content.all             	= Path.content.css  + 'all/';
Path.content.allMinified     	= Path.content.all  + '*.min.*.css';
Path.content.allCss      		= [
									Path.content.all  + '*.css',
									'!' + Path.content.allMinified
								]

Path.content.modulos            = Path.content.css + 'modulos/';
Path.content.modulosMinified 	= Path.content.modulos  + '**/*.min.*.css';
Path.content.modulosCss         = [
									Path.content.modulos  + '**/*.css',
									'!' + Path.content.modulosMinified
								]

Path.scripts.base               = Path.base + 'Scripts/';
Path.scripts.all                = Path.scripts.base + 'all/';
Path.scripts.modulos            = Path.scripts.base + 'modulos/';
Path.scripts.allMinified     	= Path.scripts.all  + '*.min.*.js';
Path.scripts.allJs				= [
									Path.scripts.all  + '*.js',
									'!' + Path.scripts.allMinified
								]
Path.scripts.modulos            = Path.scripts.base + 'modulos/';
Path.scripts.modulosMinified 	= Path.scripts.modulos  + '**/*.min.*.js';
Path.scripts.modulosJs	        = [
									Path.scripts.modulos  + '**/*.js',
									'!' + Path.scripts.modulosMinified
								]

/**
 * Ruta base donde se cargará el servidor express
 * @property serverFiles
 * @type String
 */
Path.dest.serverFiles             = Path.base + '/public';

/**
 * Ruta de los archivos estáticos (js, css, imagenes y fuentes)
 * @property serverFiles
 * @type String
 */
Path.dest.static                  = '';
Path.dest.vendors                 = Path.dest.serverFiles + Path.dest.static + '/vendors';

module.exports = Path;