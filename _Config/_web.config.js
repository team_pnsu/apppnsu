var Config = {} ;
Config = {
	environment : "LOCAL",
	version : "8734946",
	paths:{
		base	: "http://localhost:" + window.SERVER + "/",
		statics	: "http://localhost:" + window.SERVER + "/",
		vendors	: "http://localhost:" + window.SERVER + "/",
		services: "http://localhost:" + window.SERVICES + "/"
	}
}
if(Config.environment != "LOCAL")
	Config.version = ".min."+Config.version
else
	Config.version = ""